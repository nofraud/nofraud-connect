# Magento 1 Module - NoFraud Connect

--------------------
* Author: Jorge Peralta
* Email: jperalta0789@gmail.com

## Overview

--------------------
This module allows you to send transaction to NoFraud to be screened.
At the moment there are 2 modes.

* Pre-Gateway
* Post-Gateway

## Configuration

--------------------
You will find the settings for this module by going to System > Configuration > NoFraud > Connect

There you will see the settings for the module.

* __Enabled__ - The global enabler/disabler of nofraud logic.
* __NoFraud Direct API Key__ - This is the your api key that can be found in your NoFraud account.
* __Use Sandbox?__ - Controls what Url the transactions will be posted to. Production vs Sandbox.
* __Mode__ - Pregateway or postgateway.
* __Payment Methods to Skip__ - Multiselect of all enabled payment methods, any that are selected will not be sent to NoFraud.

## Modes

--------------------

Pre-Gateway - Sends information to NoFraud before sending to gateway.

* Please note that valid credit card numbers are required for pre-gateway to work. Please use the Payment Methods to Skip to skip methods that do not make a credit card number available.

Post-Gateway - Sends information to NoFraud after getting a response from the gateway.

## Installation

--------------------

Available options:

* Directly from bitbucket - download the latest release from the release section
* Through the Modman file in this repository.
