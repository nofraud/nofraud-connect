<?php
/**
 * Renderer for Authorize.net compatibility field.
 *
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Block_System_Config_Field_Authnet_Compatibility
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Authorize.net field values that are required to be compatible with NoFraud direct api requests.
     * @var array
     */
    private $_fieldValues = array(
        'Enabled' => 'payment/authorizenet/active',
        'API Login ID' => 'payment/authorizenet/login',
        'Transaction Key' => 'payment/authorizenet/trans_key',
        'Credit Cart Verification' => 'payment/authorizenet/useccv',
    );

    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '<div>';

        foreach ($this->_fieldValues as $name => $path) {
            $html .= $this->_validateField($name, $path);
        }

        $html .= '</div>';

        return $html;
    }

    /**
     * Generate success/fail html based on field value.
     *
     * @param $name
     * @param $path
     * @return string
     */
    private function _validateField($name, $path){
        $value =  Mage::getStoreConfig($path);

        $response = '<div style="background: url(/skin/adminhtml/default/default/images/error_msg_icon.gif) 0 0 no-repeat !important;text-indent: 20px;font-size: 14px;margin-bottom: 5px;">' . $name .'</div>';

        if(!empty($value)){
            $response = '<div style="background: url(/skin/adminhtml/default/default/images/fam_bullet_success.gif) 0 0 no-repeat !important;text-indent: 20px;font-size: 14px;margin-bottom: 5px;">' . $name .'</div>';
        }

        return $response;
    }
}
