<?php

/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Observer_Quote_Submit_Failure extends NoFraud_Connect_Model_Request
{
    /**
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function salesModelServiceQuoteSubmitFailure(Varien_Event_Observer $observer)
    {
        if ($this->_getNoFraudHelper()->isNoFraudEnabled()) {
            /* @var $order Mage_Sales_Model_Order */
            $order = $observer->getEvent()->getOrder();
            /* @var $payment Mage_Sales_Model_Order_Payment */
            $payment = $order->getPayment();

            $this->lastTransaction = $this->_getNoFraudHelper()->getTransactionFromSession();

            if (!empty($this->lastTransaction)) {
                $request = $this->_buildGatewayResponseRequest($this->lastTransaction['id'], $payment, $order);
                $response = $this->_sendRequest($request, $this->_getNoFraudHelper()->getURL("gateway_response"));

                if ($this->lastTransaction['decision'] == "review" && !$payment->getIsFraudDetected()) {
                    $payment->setIsTransactionPending(true);
                    $payment->setIsFraudDetected(true);
                    $order->setState($order->getState(), Mage_Sales_Model_Order::STATUS_FRAUD);
                    $payment->save();
                    $order->save();
                }
                $this->_unsetLastTransaction();
            }
        }
    }
}