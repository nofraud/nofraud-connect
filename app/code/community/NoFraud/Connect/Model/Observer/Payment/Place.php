<?php

/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Observer_Payment_Place extends NoFraud_Connect_Model_Request
{
    const STATE_COMMENT = 'NoFraud updated order status to ';
    const LOG_FILE_NAME = 'NoFraud_Exception.log';

    /**
     * @param Varien_Event_Observer $observer
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function salesOrderPaymentPlaceStart(Varien_Event_Observer $observer)
    {
        if ($this->_getNoFraudHelper()->isPreGatewayEnabled()) {
            /* @var $payment Mage_Sales_Model_Order_Payment */
            $payment = $observer->getEvent()->getPayment();
            /* @var $order Mage_Sales_Model_Order */
            $order = $payment->getOrder();

            $this->_unsetLastTransaction();

            $request = $this->_buildRequest($payment, $order);
            $result = $this->_sendRequest($request, $this->_getNoFraudHelper()->getURL());

            $this->_setOrderStatusByResult($order, $result);

            $this->_addStatusHistoryCommentToOrder($result, $order);

            if (isset($result['decision']) && $result['decision'] == "fail") {
                $this->_unsetLastTransaction();
                $message = ($result['message'] || "Declined") ? $result['message'] : "Declined";
                Mage::throwException($this->_getNoFraudHelper()->__($message));
            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function salesOrderPaymentPlaceEnd(Varien_Event_Observer $observer)
    {
        if ($this->_getNoFraudHelper()->isPostGatewayEnabled()) {
            /* @var $payment Mage_Sales_Model_Order_Payment */
            $payment = $observer->getEvent()->getPayment();
            /* @var $order Mage_Sales_Model_Order */
            $order = $payment->getOrder();

            $this->_unsetLastTransaction();

            $request = $this->_buildRequest($payment, $order, true);
            $result = $this->_sendRequest($request, $this->_getNoFraudHelper()->getURL());

            $this->_setOrderStatusByResult($order, $result);

            $this->_addStatusHistoryCommentToOrder($result, $order);
        }
    }

    /**
     * @param $result
     * @param $order Mage_Sales_Model_Order
     * @throws Exception
     */
    private function _addStatusHistoryCommentToOrder($result, $order){
        $comment = "NoFraud was unable to render a result on this transaction due to an error.";
        if (isset($result['id'])) {
            $comment = "NoFraud rendered a result of \"{$result['decision']}\" for this transaction giving it the ID of <a target=\"_blank\" href=\"https://portal.nofraud.com/transaction/{$result['id']}\">{$result['id']}</a>";
            if ($result['decision'] == "review") {
                $comment .= "\nFor Review results, we're on it already looking into it on your behalf.";
            }
            $this->_setLastTransaction($result);
        }
        if(isset($result['skip_transaction'])){
            $comment = $result['message'];
        }
        $order->addStatusHistoryComment("{$comment}");
        $order->save();
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @param $result
     * @throws Exception
     */
    private function _setOrderStatusByResult($order, $result){
        if ($this->_getNoFraudHelper()->isOrderStatusUpdateEnabled()) {

            $state = $this->_getNoFraudHelper()->getOrderStateValue($result['decision']);

            if ($state && $state !== 'no-change') {
                $this->_setOrderState($order, $state);
            }
        }
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @param $state
     */
    private function _setOrderState($order, $state){

        try{
            if($state === Mage_Sales_Model_Order::STATE_CANCELED){
                if($order->canCancel()){
                    $order->cancel();
                    $order->save();
                    return;
                }
            }
            if($state === Mage_Sales_Model_Order::STATE_HOLDED){
                if($order->canHold()){
                    $order->hold();
                    $order->save();
                    return;
                }
            }

            $order->setState($state, true, self::STATE_COMMENT . $state);
        }
        catch (Exception$e){
            Mage::log($e->getMessage(), Zend_Log::ERR, self::LOG_FILE_NAME);
        }
    }
}