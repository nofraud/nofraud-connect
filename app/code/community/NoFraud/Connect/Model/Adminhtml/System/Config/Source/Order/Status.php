<?php

/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Adminhtml_System_Config_Source_Order_Status extends Mage_Adminhtml_Model_System_Config_Source_Order_Status {
    public function toOptionArray()
    {
        $options = array();
        $options[] = array(
            'value' => 'no-change',
            'label' => Mage::helper('adminhtml')->__('No Status Update')
        );

        $states = Mage::getModel('sales/order_config')->getStates();

        foreach ($states as $code=>$label) {
            $options[] = array(
                'value' => $code,
                'label' => $label
            );
        }


        return $options;
    }
}