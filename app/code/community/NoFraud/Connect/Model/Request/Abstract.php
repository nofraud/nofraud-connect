<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Request_Abstract
{
    const DEFAULT_AVS_CODE = 'U';
    const DEFAULT_CVV_CODE = 'U';

    const GENE_BRAINTREE_METHOD = 'gene_braintree_creditcard';

    private  $_cardTypeMap = array(
        'AE' => "Amex",
        'Di' => "Discover",
        'MC' => "Mastercard",
        'VS' => "Visa",
    );

    /**
     * @param $payment Mage_Sales_Model_Order_Payment
     * @param $order Mage_Sales_Model_Order
     * @return string
     */
    protected function _getGatewayStatus($payment, $order){
        $gatewayStatus = "fail";
        if ($payment->getIsFraudDetected()) {
            $gatewayStatus = "review";
        }
        if ($order->getBaseTotalDue() == 0) {
            $gatewayStatus = "pass";
        }

        return $gatewayStatus;
    }

    /**
     * @param $payment Mage_Sales_Model_Order_Payment
     * @param $order Mage_Sales_Model_Order
     * @param $postGateway bool
     * @param $token string
     * @return array
     */
    protected function _setBaseParams($payment, $order, $token, $postGateway = false){
        $params = array();
        $params['nf-token'] = $token;
        $params['amount'] = Mage::getModel('directory/currency')->formatTxt($order->getGrandTotal(), array('display' => Zend_Currency::NO_SYMBOL));
        $params['customerIP'] = $this->_getCustomerIp($order);

        if ($order->getShippingAmount() > 0) {
            $params['shippingAmount'] = Mage::getModel('directory/currency')->formatTxt($order->getShippingAmount(), array('display' => Zend_Currency::NO_SYMBOL));
        }

        if($postGateway){
            $params['avsResultCode'] = self::DEFAULT_AVS_CODE;
            $params['cvvResultCode'] = self::DEFAULT_CVV_CODE;

            $ccAvsStatus = $payment->getCcAvsStatus();
            if (!empty($ccAvsStatus)) {
                $params['avsResultCode'] = $ccAvsStatus;
            }

            $ccCidStatus = $payment->getCcCidStatus();
            if (!empty($ccCidStatus)) {
                $params['cvvResultCode'] = $ccCidStatus;
            }

            $params = $this->_setAdditionalInformationFromPayment($params, $payment);
        }

        return $params;
    }

    /**
 	* @param $order Mage_Sales_Model_Order
 	* @return string
 	*/
 	
 	protected function _getCustomerIp($order)
 	
 	{
 		//get X_FORWARDED_FOR if exists
 		$customerIp = $order->getXForwardedFor();
 		//Only get the first IP 
 		$customerIp = explode(',', $customerIp)[0];
 		
 		//if not get customer IP
 		if (!$customerIp){
 			$customerIp =  $order->getRemoteIp();
 		}
 
 		return $customerIp;
 	
 	}


    /**
     * @param $billing Mage_Sales_Model_Order_Address
     * @return array
     */
    protected function _setBillingParams($billing){
        $billing_region = Mage::getModel('directory/region')->load($billing->getRegion_id());

        $params = array();
        $params['firstName'] = $billing->getFirstname();
        $params['lastName'] = $billing->getLastname();
        $params['address'] = $billing->getStreet(1) . " " . $billing->getStreet(2);
        $params['city'] = $billing->getCity();
        $params['state'] = $billing_region->getCode();
        $params['zip'] = $billing->getPostcode();
        $params['country'] = $billing->getCountry();
        $params['phoneNumber'] = $billing->getTelephone();

        if (!is_null($billing->getCompany())) {
            $params['company'] = $billing->getCompany();
        }

        return $params;
    }

    /**
     * @param $shipping Mage_Sales_Model_Order_Address
     * @return array
     */
    protected function _setShippingParams($shipping){

        $shipping_region = Mage::getModel('directory/region')->load($shipping->getRegion_id());
        $params = array();
        $params['firstName'] = $shipping->getFirstname();
        $params['lastName'] = $shipping->getLastname();
        $params['address'] = $shipping->getStreet(1) . " " . $shipping->getStreet(2);
        $params['city'] = $shipping->getCity();
        $params['state'] = $shipping_region->getCode();
        $params['zip'] = $shipping->getPostcode();
        $params['country'] = $shipping->getCountry();

        if (!is_null($shipping->getCompany())) {
            $params['company'] = $shipping->getCompany();
        }

        return $params;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    protected function _setCustomerParams($order){
        $params = array();
        $params['email'] = $order->getCustomerEmail();

        return $params;
    }

    /**
     * @param $payment Mage_Sales_Model_Order_Payment
     * @return array
     */
    protected function _setPaymentParams($payment){

        $CcData = $payment->getCcData();
        if (!empty($CcData)) {
            $payment = $CcData;
        }

        $params = array();

        $ccNumber = $payment->getCcNumber();
        if (!empty($ccNumber)) {
            $params['creditCard']['cardNumber'] = $this->_getCcTypeByCodeMap($ccNumber);
        }

        $ccType = $payment->getCcType();
        if (!empty($ccType)){
            $params['creditCard']['cardType'] = $ccType;
        }

        $expirationDate = $this->_getExpirationDate($payment);
        if(!empty($expirationDate)){
            $params['creditCard']['expirationDate'] = $expirationDate;
        }

        $ccCid = $payment->getCcCid();
        if (!empty($ccCid)) {
            $params['creditCard']['cardCode'] = $ccCid;
        }

        $ccLast4 = $payment->getCcLast4();
        if (!empty($ccLast4)) {
            $params['creditCard']['last4'] = $ccLast4;
        }

        if(empty($params)){
            return $this->_defaultPaymentParams();
        }

        return $params;
    }


    /**
     * Handle additional data for specific payment methods
     *
     * @param $params array
     * @param $payment Mage_Sales_Model_Order_Payment
     * @return array
     */
    private function _setAdditionalInformationFromPayment($params, $payment){
        $additionalInfo = $payment->getAdditionalInformation();
        $paymentMethod = $payment->getMethod();

        switch ($paymentMethod){
            case(self::GENE_BRAINTREE_METHOD):
                $sAvs = array_key_exists('avsStreetAddressResponseCode', $additionalInfo) ? $additionalInfo['avsStreetAddressResponseCode'] : NULL; // AVS Street Address Match
                $zAvs = array_key_exists('avsPostalCodeResponseCode', $additionalInfo) ? $additionalInfo['avsPostalCodeResponseCode'] : NULL; // AVS Street Address Match

                $cvv = array_key_exists('cvvResponseCode', $additionalInfo) ? $additionalInfo['cvvResponseCode'] : NULL; // AVS Street Address Match

                if(!is_null($sAvs) || !is_null($zAvs)){
                    $params['avsResultCode'] = $sAvs . $zAvs;
                }

                if(!is_null($cvv)){
                    $params['cvvResultCode'] = $cvv;
                }
                break;

            default:
                break;
        }

        return $params;
    }

    /**
     * @return array
     */
    private function _defaultPaymentParams(){
        $params = array();

        $params['creditCard']['cardNumber'] = '0000000000000000';
        $params['creditCard']['expirationDate'] = '0000';
        $params['creditCard']['cardCode'] = '000';
        $params['creditCard']['last4'] = '0000';

        return $params;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    protected function _setOrderParams($order){
        $params = array();
        $params['invoiceNumber'] = $order->getIncrementId();

        return $params;
    }

    /**
     * Returns expiration date in the following formats:
     * MM/YYYY or MM/YY
     *
     * @param $payment Mage_Sales_Model_Order_Payment
     * @return string
     */
    protected function _getExpirationDate($payment){
        $expirationMonth = $payment->getCcExpMonth();
        $expirationYear = $payment->getCcExpYear();

        if($expirationMonth == '0' || $expirationYear == '0'){
            return false;
        }

        $expirationDate = $expirationMonth;
        if (strlen($expirationMonth) == 1) {
            $expirationDate = "0" . $expirationMonth;
        }

        if (strlen($expirationYear) > 4) {
            return false;
        }

        $expirationDate .= '/' . $expirationYear;

        return $expirationDate;
    }

    /**
     * Get Credit Card Type by code.
     * @param $code
     * @return bool|mixed
     */
    protected function _getCcTypeByCodeMap($code){
        if(!isset($this->_cardTypeMap[$code])){
            return $code;
        }

        return $this->_cardTypeMap[$code];
    }

    /**
     * @param $request
     * @param $nfurl
     * @return mixed
     */
    protected function _sendRequest($request, $nfurl)
    {
        if(isset($request['skip_transaction'])) {
            return $request;
        }

        $body = json_encode($request);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_URL, $nfurl);
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($body)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $result = curl_exec($ch);
        curl_close($ch);

        $res_obj = json_decode($result, true);

        return $res_obj;
    }
}