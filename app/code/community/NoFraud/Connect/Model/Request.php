<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Request extends NoFraud_Connect_Model_Request_Abstract
{
    /**
     * @var array
     */
    protected $lastTransaction;

    /**
     * @var NoFraud_Connect_Helper_Data
     */
    private $_helper;

    /**
     * Get NoFraud helper.
     *
     * @return Mage_Core_Helper_Abstract|NoFraud_Connect_Helper_Data
     */
    protected function _getNoFraudHelper(){
        if(is_null($this->_helper)){
            $this->_helper = Mage::helper('nofraud_connect');
        }
        return $this->_helper;
    }

    /**
     * @param $id string
     * @param $payment Mage_Sales_Model_Order_Payment
     * @param $order Mage_Sales_Model_Order
     * @return array|bool
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _buildGatewayResponseRequest($id, $payment, $order)
    {
        if($this->_isMethodInSkipList($payment->getMethod())){
            return false;
        }

        $gateway_status = $this->_getGatewayStatus($payment, $order);

        $params = array();
        $params['nf-token'] = $this->_getNoFraudHelper()->getConfigData('nftoken');
        $params['nf-id'] = $id;
        $params['gateway-response'] = array();
        $params['gateway-response']['result'] = $gateway_status;

        $lastTransId = $payment->getLastTransId();
        if (!empty($lastTransId)) {
            $params['gateway-response']['transaction-id'] = $lastTransId;
        }

        return $params;
    }

    /**
     * @param $payment Mage_Sales_Model_Order_Payment
     * @param $order Mage_Sales_Model_Order
     * @param $postGateway bool
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _buildRequest($payment, $order, $postGateway = false)
    {
        if($this->_isMethodInSkipList($payment->getMethod())){
            return $this->_getSkipMessage();
        }

        $billing = $order->getBillingAddress();
        $shipping = $order->getShippingAddress();

        $nftoken = $this->_getNoFraudHelper()->getConfigData('nftoken');

        $params = $this->_setBaseParams($payment, $order, $nftoken, $postGateway);

        $params['billTo'] = $this->_setBillingParams($billing);

        if (!empty($shipping)) {
            $params['shipTo'] = $this->_setShippingParams($shipping);
        }

        $params['customer'] = $this->_setCustomerParams($order);

        $params['payment'] = $this->_setPaymentParams($payment);

        $params['order'] = $this->_setOrderParams($order);

        return $params;
    }

    /**
     * Verifies if payment method is in skip list.
     *
     * @param $method
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    private function _isMethodInSkipList($method){
        $methodList = $this->_getNoFraudHelper()->getDisabledGatewayList();

        if(in_array($method, $methodList)){
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    private function _getSkipMessage(){
        $result['skip_transaction'] = true;
        $result['message'] = "Payment method is in skip list. Transaction data was not sent to NoFraud.";

        return $result;
    }

    /**
     * Unset last transaction variable and from session.
     */
    protected function _unsetLastTransaction(){
        $this->lastTransaction = "";
        $this->_getNoFraudHelper()->unsetTransactionFromSession();
    }


    /**
     * Set last transaction variable and in session.
     * @param $value array
     */
    protected function _setLastTransaction($value){
        $this->lastTransaction = $value;
        $this->_getNoFraudHelper()->setTransactionToSession($value);
    }
}