<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_System_Config_Source_Mode
{
    const NOFRAUD_MODE_PRE = 'Pre-Gateway';
    const NOFRAUD_MODE_POST = 'Post-Gateway';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label'=>Mage::helper('nofraud_connect')->__(self::NOFRAUD_MODE_PRE)),
            array('value' => 1, 'label'=>Mage::helper('nofraud_connect')->__(self::NOFRAUD_MODE_POST)),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('nofraud_connect')->__(self::NOFRAUD_MODE_PRE),
            1 => Mage::helper('nofraud_connect')->__(self::NOFRAUD_MODE_POST),
        );
    }
}