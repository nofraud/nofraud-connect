<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_System_Config_Payment_Methods
{
    protected $_options;

    /**
     * @param bool $isMultiselect
     * @return mixed
     */
    public function toOptionArray($isMultiselect=false)
    {
        if (!$this->_options) {
            $this->_options = $this->_getEnabledPaymentMethodsAsArray();
        }

        if(!$isMultiselect){
            array_unshift($this->_options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
        }

        return $this->_options;
    }

    /**
     * Convert active methods collection to option array.
     *
     * @return array
     */
    private function _getEnabledPaymentMethodsAsArray()
    {
        $paymentMethods = Mage::getModel('payment/config')->getActiveMethods();

        $optionArray = array();

        /** @var $option Mage_Payment_Model_Method_Abstract*/
        foreach ($paymentMethods as $paymentMethod) {
            $optionArray[] = array(
                'value' => $paymentMethod->getCode(),
                'label' => $paymentMethod->getTitle(),
            );
        }

        return $optionArray;
    }
}