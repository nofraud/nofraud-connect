<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Model_Compatibility_Authorizenet extends Mage_Paygate_Model_Authorizenet
{
    /**
     * Sets card data into additional information of payment model
     *
     * @param varien_Object $response
     * @param Mage_Sales_Model_Order_Payment $payment
     * @return varien_object
     */
    protected function _registerCard(varien_Object $response, Mage_Sales_Model_Order_Payment $payment)
    {
        $card = parent::_registercard($response, $payment);
        $card->setCcAvsResultCode($response->getAvsResultCode());
        $card->setCcResponseCode($response->getCardCodeResponseCode());
        $payment->setCcAvsStatus($response->getAvsResultCode());
        $payment->setCcCidStatus($response->getCardCodeResponseCode());
        $payment->setCcData($card);
        return $card;
    }
}
