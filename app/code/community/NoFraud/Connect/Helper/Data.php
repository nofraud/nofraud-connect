<?php
/**
 * @category    NoFraud
 * @package     NoFraud_Connect
 * @author      Jorge Peralta (j.peralta@nofraud.com)
 * @copyright   Copyright (c) 2018 NoFraud (https://www.nofraud.com/)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class NoFraud_Connect_Helper_Data extends Mage_Core_Helper_Abstract
{
    const NOFRAUD_LOG_NAME = 'nofraud-connect.log';
    const NOFRAUD_SESSION_KEY = 'NfLastTransaction';
    const NOFRAUD_SECTION = 'nofraud';
    const NOFRAUD_GROUP = 'connect';
    const NOFRAUD_FIELD_ACTIVE = 'active';
    const NOFRAUD_FIELD_SANDBOX = 'sandbox';
    const NOFRAUD_FIELD_MODE = 'mode';
    const NOFRAUD_FIELD_METHODS = 'methods';
    const NOFRAUD_FIELD_UPDATE_ORDER_STATUS = 'status_update';
    const NOFRAUD_FIELD_ORDER_STATUS_PASS = 'passed';
    const NOFRAUD_FIELD_ORDER_STATUS_FAIL = 'failed';
    const NOFRAUD_FIELD_ORDER_STATUS_REVIEW = 'review';
    const NOFRAUD_URL = 'https://api.nofraud.com/';
    const NOFRAUD_TEST_URL = 'https://apitest.nofraud.com/';

    /**
     * Stores enabled state.
     * @var bool
     */
    protected $_isNoFraudEnabled;

    /**
     * Get test/production url depending on settings.
     *
     * @param string $addition
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getURL($addition = "")
    {
        $url = self::NOFRAUD_URL . $addition;

        if ($this->getConfigData(self::NOFRAUD_FIELD_SANDBOX)) {
            $url = self::NOFRAUD_TEST_URL . $addition;
        }

        return $url;
    }

    /**
     * Get config data for specified field.
     *
     * @param $field
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getConfigData($field)
    {
        $path = self::NOFRAUD_SECTION . '/' . self::NOFRAUD_GROUP . '/' . $field;
        return Mage::getStoreConfig($path, Mage::app()->getStore());
    }

    /**
     * Check if nofraud is enabled in settings.
     *
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isNoFraudEnabled()
    {
        if(is_null($this->_isNoFraudEnabled)){
            $this->_isNoFraudEnabled = $this->getConfigData(self::NOFRAUD_FIELD_ACTIVE);
        }
        return $this->_isNoFraudEnabled;
    }

    /**
     * Check if Pre-Gateway is enabled in settings.
     *
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isPreGatewayEnabled()
    {
        if(!$this->isNoFraudEnabled()){
            return false;
        }

        $mode = $this->getConfigData(self::NOFRAUD_FIELD_MODE);

        return $mode == 0 ? true : false;
    }

    /**
     * Check if Post-Gateway is enabled in settings.
     *
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isPostGatewayEnabled()
    {
        if(!$this->isNoFraudEnabled()){
            return false;
        }

        $mode = $this->getConfigData(self::NOFRAUD_FIELD_MODE);

        return $mode == 1 ? true : false;
    }

    /**
     * Get an array of all skipped gateways.
     *
     * @return array|bool|mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getDisabledGatewayList(){
        if(!$this->isNoFraudEnabled()){
            return false;
        }

        $methods = $this->getConfigData(self::NOFRAUD_FIELD_METHODS);
        $methods = explode(',',$methods);

        return $methods;
    }

    /**
     * Log text to NoFraud custom log.
     *
     * @param $text
     * @param int $level
     */
    public function log($text, $level = Zend_Log::ALERT){
        Mage::log($text, $level, self::NOFRAUD_LOG_NAME);
    }

    /**
     * Set transaction data to session.
     *
     * @param $data
     */
    public function setTransactionToSession($data){
        Mage::getSingleton('core/session')->setData(self::NOFRAUD_SESSION_KEY, $data);
    }

    /**
     * Get transaction data from session.
     *
     * @return mixed
     */
    public function getTransactionFromSession(){
        return Mage::getSingleton('core/session')->getData(self::NOFRAUD_SESSION_KEY);
    }

    /**
     * Clear transaction data from session.
     */
    public function unsetTransactionFromSession(){
        Mage::getSingleton('core/session')->unsetData(self::NOFRAUD_SESSION_KEY);
    }


    /**
     * Check if order update is enabled.
     *
     * @return bool|mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isOrderStatusUpdateEnabled()
    {
        if(!$this->isNoFraudEnabled()){
            return false;
        }

        return $this->getConfigData(self::NOFRAUD_FIELD_UPDATE_ORDER_STATUS);
    }

    /**
     * Get mapped state update value.
     *
     * @param $state string
     * @return bool|mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getOrderStateValue($state)
    {
        if(!$this->isNoFraudEnabled()){
            return false;
        }

        switch ($state){
            case ('pass'):
                $config = self::NOFRAUD_FIELD_ORDER_STATUS_PASS;
                break;
            case ('fail'):
                $config = self::NOFRAUD_FIELD_ORDER_STATUS_FAIL;
                break;
            case ('review'):
                $config = self::NOFRAUD_FIELD_ORDER_STATUS_REVIEW;
                break;
            default:
                return false;
                break;
        }

        return $this->getConfigData($config);
    }
} 